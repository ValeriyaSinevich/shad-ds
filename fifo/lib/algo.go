package lib

// This is a PerfectLink - every message is eventually delivered exactly once.
type UnderlyingTransport interface {
	SendMessage(message Message, destination int)
}

type Message interface {
	Value() string
}

// FifoTransport is interface implemented by FifoPerfectLink.
type FifoTransport interface {
	// SendMessage sends message value to destination.
	SendMessage(value string, destination int)

	// OnMessage is called by underlying transport.
	OnMessage(message Message, source int)

	// ReceivedValues returns messages delivered so far.
	ReceivedValues(source int) []string
}

type FifoTransportCtor func(nodeId int, transport UnderlyingTransport) FifoTransport
