package lib

import (
	"container/heap"
	"fmt"
	"sort"
	"testing"
	"time"
)

type TestTransport struct {
	source    int
	messages  []SentMessage
	timestamp *int
}

func (transport *TestTransport) SendMessage(message Message, destination int) {
	sentMessage := SentMessage{
		Message:          message,
		Source:           transport.source,
		Destination:      destination,
		SendTimestamp:    *transport.timestamp,
		DeliverTimestamp: *transport.timestamp + 1}
	transport.messages = append(transport.messages, sentMessage)
}

func (transport TestTransport) SentMessages() []SentMessage {
	return transport.messages
}

func (transport *TestTransport) ClearSentMessages() {
	transport.messages = transport.messages[:0]
}

//////////////////////////////////////////////////////////////////////////////////

type DeliveryQueue []SentMessage

func (q DeliveryQueue) Len() int { return len(q) }

func (q DeliveryQueue) Less(i, j int) bool {
	return q[i].DeliverTimestamp < q[j].DeliverTimestamp
}

func (q DeliveryQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

func (q *DeliveryQueue) Push(x interface{}) {
	sentMessage := x.(SentMessage)
	*q = append(*q, sentMessage)
}

func (q *DeliveryQueue) Pop() interface{} {
	old := *q
	n := len(old)
	item := old[n-1]
	*q = old[0 : n-1]
	return item
}

//////////////////////////////////////////////////////////////////////////////////

func CheckSendRequest(sendRequest SendRequest, nodeCount int) (err error) {
	if sendRequest.Timestamp < 0 || sendRequest.Timestamp > MaxSendTimestamp {
		return fmt.Errorf("Invalid test: invalid send request timestamp (SendRequest: %v)",
				sendRequest.Timestamp)
	}

	if sendRequest.Source < 0 || sendRequest.Source >= nodeCount {
		return fmt.Errorf("Invalid test: invalid send request source (SendRequest: %v, NodeCount: %v)",
				sendRequest.Timestamp,
				nodeCount)
	}

	if sendRequest.Destination < 0 || sendRequest.Destination >= nodeCount {
		return fmt.Errorf("Invalid test: invalid send request destination (SendRequest: %v, NodeCount: %v)",
				sendRequest.Timestamp,
				nodeCount)
	}

	return  nil
}

func CheckValidator(validator Validator, nodeCount int) (err error) {
	if validator.Timestamp < 0 || validator.Timestamp > MaxTimestamp {
		return fmt.Errorf("Invalid test: invalid validator timestamp (Validator: %v)",
				validator)
	}

	if validator.Node < 0 || validator.Node >= nodeCount {
		return fmt.Errorf("Invalid test: invalid validator node (Validator: %v, NodeCount: %v)",
				validator,
				nodeCount)
	}

	if validator.Source < 0 || validator.Source >= nodeCount {
		return fmt.Errorf("Invalid test: invalid validator source (Validator: %v, NodeCount: %v)",
				validator,
				nodeCount)
	}

	return nil
}

func RunTest(test TestDescriptor, nodeCtor FifoTransportCtor) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("%v", e)
		}
	}()

	if test.NodeCount < 1 {
		return fmt.Errorf("Invalid test: NodeCount must be greater than zero (NodeCount: %v)",
			test.NodeCount)
	}

	if test.NodeCount > MaxNodeCount {
		return fmt.Errorf("Invalid test: NodeCount must not be greater than MaxNodeCount (NodeCount: %v)",
			test.NodeCount)
	}

	for _, validator := range test.Validators {
		if err = CheckValidator(validator, test.NodeCount); err != nil {
			return err
		}
	}

	for _, sendRequest := range test.SendRequests {
		if err = CheckSendRequest(sendRequest, test.NodeCount); err != nil {
			return err
		}
	}

	nodes := make([]FifoTransport, test.NodeCount)
	transports := make([]*TestTransport, test.NodeCount)

	timestamp := 0

	for i := 0; i < test.NodeCount; i++ {
		transports[i] = &TestTransport{source: i, timestamp: &timestamp}
		nodes[i] = nodeCtor(i, transports[i])
	}

	sort.Slice(test.Validators, func(i, j int) bool {
		return test.Validators[i].Timestamp < test.Validators[j].Timestamp
	})

	sort.Slice(test.SendRequests, func(i, j int) bool {
		if test.SendRequests[i].Timestamp != test.SendRequests[j].Timestamp {
			return test.SendRequests[i].Timestamp < test.SendRequests[j].Timestamp
		}

		if test.SendRequests[i].Source != test.SendRequests[j].Source {
			return test.SendRequests[i].Source < test.SendRequests[j].Source
		}

		if test.SendRequests[i].Destination != test.SendRequests[j].Destination {
			return test.SendRequests[i].Destination < test.SendRequests[j].Destination
		}

		return true
	})

	for i := 0; i < len(test.SendRequests)-1; i++ {
		current := test.SendRequests[i]
		next := test.SendRequests[i+1]

		if current.Timestamp == next.Timestamp && current.Source == next.Source && current.Destination == next.Destination {
			return fmt.Errorf("Invalid test: ambigious send request from %v to %v at timestamp %v",
				current.Source,
				current.Destination,
				current.Timestamp)
		}
	}

	deliveryQueue := make(DeliveryQueue, 0, 10)
	heap.Init(&deliveryQueue)

	for timestamp < MaxTimestamp {
		nextTimestamp := MaxTimestamp
		if len(test.Validators) > 0 && test.Validators[0].Timestamp < nextTimestamp {
			nextTimestamp = test.Validators[0].Timestamp
		}

		if len(test.SendRequests) > 0 && test.SendRequests[0].Timestamp < nextTimestamp {
			nextTimestamp = test.SendRequests[0].Timestamp
		}

		if len(deliveryQueue) > 0 && deliveryQueue[0].DeliverTimestamp < nextTimestamp {
			nextTimestamp = deliveryQueue[0].DeliverTimestamp
		}

		timestamp = nextTimestamp

		for len(deliveryQueue) > 0 && deliveryQueue[0].DeliverTimestamp == timestamp {
			sentMessage := heap.Pop(&deliveryQueue).(SentMessage)
			nodes[sentMessage.Destination].OnMessage(sentMessage.Message, sentMessage.Source)
		}

		for len(test.SendRequests) > 0 && test.SendRequests[0].Timestamp == timestamp {
			request := test.SendRequests[0]

			if request.Timestamp > MaxSendTimestamp {
				return fmt.Errorf("Invalid test: send request timestamp is greater than MaxSendTimestamp (%v > %v)",
					request.Timestamp,
					MaxSendTimestamp)
			}

			nodes[request.Source].SendMessage(request.Value, request.Destination)
			test.SendRequests = test.SendRequests[1:]
		}

		for len(test.Validators) > 0 && test.Validators[0].Timestamp == timestamp {
			validator := test.Validators[0]
			values := nodes[validator.Node].ReceivedValues(validator.Source)
			if !validator.Validate(values) {
				return fmt.Errorf("Algorithm validation failed (Timestamp: %v, Node: %v, Source: %v, ReceivedValues %v)",
					validator.Timestamp,
					validator.Node,
					validator.Source,
					values)
			}
			test.Validators = test.Validators[1:]
		}

		for _, transport := range transports {
			for _, message := range transport.SentMessages() {
				message.DeliverTimestamp = test.DelayFunc(message)

				if message.DeliverTimestamp <= timestamp {
					return fmt.Errorf("Invalid test: message handler set negative deliver delay (SentMessage: %+v)",
						message)
				}

				if message.DeliverTimestamp-message.SendTimestamp > MaxMessageDelay {
					return fmt.Errorf("Invalid test: deliver delay is greater than MaxDeliverDelay (SentMessage: %+v)",
						message)
				}

				heap.Push(&deliveryQueue, message)
			}
			transport.ClearSentMessages()
		}
	}

	return nil
}

func RunTests(t *testing.T, algo FifoTransportCtor, tests []TestDescriptor, shouldFail bool) {
	var failed bool

	for _, test := range tests {
		t.Run(fmt.Sprintf("%q by %s", test.TestName, test.Author), func(t *testing.T) {
			resultChannel := make(chan error)
			go func() {
				resultChannel <- RunTest(test, algo)
			}()

			select {
			case err := <-resultChannel:
				if err != nil {
					failed = true
				}
					
				if failed && !shouldFail {
					t.Fatalf("Test failed: %v", err)	
				}

			case <-time.After(TestTimeout):
				t.Fatalf("Test timed out")
			}
		})
	}

	if shouldFail && !failed {
		t.Fatalf("None of the tests failed")
	}
}
