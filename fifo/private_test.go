// +build private

package fifo

import (
	"gitlab.com/slon/shad-ds/fifo/algo"
	"gitlab.com/slon/shad-ds/fifo/lib"
	"gitlab.com/slon/shad-ds/fifo/tests"
	algo2 "gitlab.com/slon/shad-ds/private/fifo/algo"
	tests2 "gitlab.com/slon/shad-ds/private/fifo/tests"
	"testing"
)

func TestExampleIsNotPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExtraTests, true)
}

func TestSolutionIsPassingPrivateTests(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tests2.Tests, false)
}

func TestReferenceIsPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo2.New, tests.ExtraTests, false)
}
