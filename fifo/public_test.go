package fifo

import (
	"gitlab.com/slon/shad-ds/fifo/algo"
	"gitlab.com/slon/shad-ds/fifo/lib"
	"gitlab.com/slon/shad-ds/fifo/tests"
	"testing"
)

func TestExampleIsPassingTests(t *testing.T) {
	lib.RunTests(t, algo.NewExample, tests.ExampleTests, false)
}

func TestSolutionIsPassingExampleTests(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tests.ExampleTests, false)
}

func TestSolutionIsPassingExtraTests(t *testing.T) {
	lib.RunTests(t, algo.NewSolution, tests.ExtraTests, false)
}
